import math as mt
def Mean(numbers):
    sum = 0
    n = len(numbers)
    for number in numbers:
        sum = sum + number

    return sum/n

def Var(numbers):
    mean = Mean(numbers)
    n = len(numbers)
    squaredSum = 0
    for number in numbers:
        squaredSum = squaredSum + (number - mean)**2
        
    return squaredSum / n
    
def Sd(numbers):
    var = Var(numbers)
    return mt.sqrt(var)
    
def Var2(numbers):
    squaredMean = Mean(numbers) * Mean(numbers)
    n = len(numbers)
    squaredSum = 0
    for number in numbers:
        squaredSum = squaredSum + (number ** 2)

    meanSquaredSum = squaredSum / n
    return (meanSquaredSum - squaredMean)