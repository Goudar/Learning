# learning about beautiful soup
# urllib.request - lib to open URLs into HTML
# bs4 to parse the opened html
from urllib.request import urlopen
from bs4 import BeautifulSoup as bs

#define URL variable to have the link to target page
url = 'https://www.thehindu.com/tag/1349/politics/'
html = urlopen(url) # now we got the HTML page

# to parse above HTML, we need BeautifulSoup object which can be created as follows:
page = bs(html, 'lxml')

# getting title
print(page.title)

#print(page.find_all("span",class_="fts-menu"))
trendingToday = page.find_all("span", class_="fts-menu")
print(page.contents)

#to get the html into text
text = page.get_text()

