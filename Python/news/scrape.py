# library for opening URLs
from article import Article
from urllib.request import urlopen
# library to parse HTML
from bs4 import BeautifulSoup as bs

# page link to scrape
url = "https://inshorts.com/en/read/politics"

htmlPage = urlopen(url)
# creating soup object with the page
page = bs(htmlPage, "lxml")

#find all div containing news
newsArticles = page.find_all("div", class_="news-card")

# list of article objects
newsArticlesList = []

for nArticle in newsArticles:
    title = nArticle.find('a').span.string
    author = nArticle.find('span', class_="author").string
    timePublished = nArticle.find('span', class_="time").string
    date = nArticle.find(attrs={"clas": "date"}).string
    bodyDiv = nArticle.find('div', class_="news-card-content")
    body = bodyDiv.div.string
    link = nArticle.find('a', class_="source").get('href')
    newsArticlesList.append(Article(title, body,author, date, link, timePublished))


print(len(newsArticlesList))
