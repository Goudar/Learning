
class Article:
     def __init__(self,title,body, author, publishedOn, link, time):
        self.title = title
        self.publishedOn = publishedOn
        self.body = body
        self.author = author
        self.link = link
        self.time = time
        
     def getSelfDictionary(self):
        selfDict = {}
        selfDict['title'] = self.title
        selfDict['body'] = self.body
        selfDict['publishedOn']  = self.publishedOn
        selfDict['author'] = self.author
        selfDict['link'] = self.link
        selfDict['publishedTime'] = self.time