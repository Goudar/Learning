class stack:
    top = 0
    element_list = []
    size = 0
    def __init__(self,size):
        self.size = size
        print("Stack is created with a size of: " + str(self.size))

    def push(self, element):
        if self.top == self.size:
            print("Stack is full, insertion is not possible.")
            return
        else:
            self.top = self.top + 1
            self.element_list.append(element)
#            self.printStack()

    def printStack(self):
        st = ''
        for i in self.element_list:
            st = st + str(i) + ' '
        print(st)

    def pop(self):
        if self.top == -1:
            print("Stack is empty, no deletion is possible.")
            return
        else:
            self.element_list.pop()
            print("New contents of stack: ")
            self.top = self.top - 1
            self.printStack()
a = stack(5)
print("Inserting 5 elements:")
for i in range(5):
    a.push(i*2)
a.printStack()
print("Inserting 6th element:")
a.push(10)
print("Removing element by element: ")
a.pop()
a.pop()
