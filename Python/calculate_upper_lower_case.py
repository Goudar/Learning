string = input('Enter the sentence: ')
x = 0
y = 0

for ch in string:
    if ch != ' ' and ch.isdigit() == False:
        if ch.islower():
            x = x + 1
        else:
            if ch.isupper():
                y = y + 1

print('No of upper case letters: ' + str(y))
print('No of lower case letters: ' + str(x))
