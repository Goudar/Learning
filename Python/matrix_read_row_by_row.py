#row_elements = input("Enter row elements: ")

def rotate_90_degrees(matrix, rows, columns):
    new_columns = rows
    new_rows = columns
    new_matrix = []
    column_decr = columns - 1
    for new_row in range(new_rows):
        new_row_elements = []
        row_incr = 0
        while row_incr < rows:
            new_row_elements.append(matrix[row_incr][column_decr])
            row_incr += 1
        column_decr -= 1
        new_matrix.append(new_row_elements)
    
    print(new_matrix)


matrix = []
rows = int(input("Enter no of rows in matrix: "))
columns = int(input("Enter no of columns in matrix: "))
for row in range(rows):
    row_elements = input("Enter " + str(columns) + " elements for row " + str(row+1) + ": ")
    row_list = []
    for ele in row_elements:
        if ele != " ":
            row_list.append(int(ele))
    #   print(ele)
    matrix.append(row_list)

rotate_90_degrees(matrix, rows, columns)